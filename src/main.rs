extern crate crc32fast;
extern crate reqwest;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate walkdir;

use chrono::prelude::*;
use crc32fast::Hasher;
use reqwest::header::{HeaderMap, HeaderName, HeaderValue, AUTHORIZATION, CONTENT_TYPE};
use std::collections::HashMap;
use std::env;
use std::fs;
use std::fs::{File, OpenOptions};
use std::io;
use std::io::prelude::*;
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};
use walkdir::{DirEntry, WalkDir};

#[derive(Debug, Deserialize)]
struct Entry {
    id: String,
    is_downloadable: bool,
    name: String,
    size: u64,
    server_modified: DateTime<Utc>,
}

#[derive(Debug, Deserialize, Serialize)]
struct UploadEntry {
    path: String,
    autorename: bool,
    mute: bool,
    client_modified: DateTime<Utc>,
}

#[derive(Debug, Deserialize)]
struct Entries {
    cursor: String,
    entries: Vec<Entry>,
    has_more: bool,
}

impl Default for Entry {
    fn default() -> Entry {
        Entry {
            id: String::new(),
            is_downloadable: false,
            name: String::new(),
            size: 0,
            server_modified: Utc::now(),
        }
    }
}

impl Default for Entries {
    fn default() -> Entries {
        Entries {
            cursor: String::new(),
            entries: Vec::new(),
            has_more: false,
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
struct StartSession {
    close: bool,
}

#[derive(Debug, Deserialize, Serialize)]
struct Cursor {
    session_id: String,
    offset: u64,
}

#[derive(Debug, Deserialize, Serialize)]
struct AppendSession {
    cursor: Cursor,
    close: bool,
}

#[derive(Debug, Deserialize, Serialize)]
struct EndSession {
    cursor: Cursor,
    commit: UploadEntry,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
struct Session {
    session_id: String,
}

struct Args {
    action: String,
    outdir: String,
    indir: String,
    key: String,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut args = pico_args::Arguments::from_env();
    // Arguments can be parsed in any order.
    let args = Args {
        action: args
            .value_from_str("--action")
            .unwrap_or(String::from("none")),
        outdir: args.opt_value_from_str("-o")?.unwrap_or(String::new()),
        indir: args.value_from_str("-i").unwrap_or(String::new()),
        key: args.opt_value_from_str("--key")?.unwrap_or(get_key()),
    };

    match args.action.as_ref() {
        "upload" => {
            start_upload(&args.key, &args.indir)?;
        }
        "download" => {
            start_download(&args.key, &args.outdir)?;
        }
        _ => {}
    }

    Ok(())
}

fn start_upload(key: &str, indir: &str) -> Result<(), Box<dyn std::error::Error>> {
    let checksum_file = "checksums";
    let mut checksums = HashMap::new();

    if indir.is_empty() {
        panic!("You must supply a source directory for your images");
    }

    if let Ok(file) = File::open(checksum_file) {
        let reader = BufReader::new(file);

        for (_, line) in reader.lines().enumerate() {
            let line = line.unwrap();

            let splits: Vec<&str> = line.split("|").collect();

            if let Some(checksum) = splits.get(0) {
                if let Some(path) = splits.get(1) {
                    checksums.insert(checksum.parse::<u32>().unwrap(), path.to_string());
                }
            }
        }
    }

    for entry in WalkDir::new(indir)
        .into_iter()
        .filter_entry(|e| !is_hidden(e))
    {
        let path = entry.unwrap().into_path();

        let chunk_size = 4 * 1024 * 1024;

        if let Ok(metadata) = fs::metadata(&path) {
            if metadata.is_file() {
                let file_len = metadata.len();

                let os_path = path.clone().into_os_string();
                let os_path = os_path.into_string().unwrap();

                let os_path = os_path.replace(indir, "/photos/");
                let os_path = os_path.replace("\\", "/");

                let created = metadata.created()?;
                let created = created.elapsed().unwrap().as_secs();
                let created = Utc::now() - chrono::Duration::seconds(created as i64);
                let created = Utc
                    .ymd(created.year(), created.month(), created.day())
                    .and_hms(created.hour(), created.minute(), created.second());

                let upload = UploadEntry {
                    path: os_path.clone(),
                    autorename: true,
                    mute: true,
                    client_modified: created,
                };

                let mut hasher = Hasher::new();
                hasher.update(&fs::read(&path).unwrap());
                let checksum = hasher.finalize();

                if !checksums.contains_key(&checksum) {
                    let client = reqwest::blocking::Client::new();
                    let url = reqwest::Url::parse("https://content.dropboxapi.com/2/files/upload")
                        .expect("Failed to parse url");

                    if file_len < chunk_size {
                        let _response = client
                            .post(url)
                            .headers(construct_upload_headers(
                                key,
                                &serde_json::to_string(&upload).unwrap(),
                            ))
                            .body(fs::read(&path).unwrap())
                            .send()
                            .expect("Failed to send request");
                    } else {
                        let start_url = reqwest::Url::parse(
                            "https://content.dropboxapi.com/2/files/upload_session/start",
                        )
                        .expect("Failed to parse url");

                        let start = client
                            .post(start_url)
                            .headers(construct_session_headers(
                                key,
                                &serde_json::to_string(&StartSession { close: false }).unwrap(),
                            ))
                            .send()
                            .expect("Failed to send request");

                        let session = start.json::<Session>().unwrap();

                        let mut file = File::open(&path)?;
                        let mut offset = 0;
                        loop {
                            let mut chunk = Vec::with_capacity(chunk_size as usize);
                            let n = Read::by_ref(&mut file)
                                .take(chunk_size as u64)
                                .read_to_end(&mut chunk)?;

                            if n == 0 {
                                break;
                            }

                            if n < chunk_size as usize {
                                break;
                            }

                            append(key, session.clone(), offset, chunk);
                            offset += chunk_size;
                        }

                        let commit = EndSession {
                            cursor: Cursor {
                                session_id: session.session_id,
                                offset: offset,
                            },
                            commit: upload,
                        };

                        let finish_url = reqwest::Url::parse(
                            "https://content.dropboxapi.com/2/files/upload_session/finish",
                        )
                        .expect("Failed to parse url");

                        let _finish = client
                            .post(finish_url)
                            .headers(construct_session_headers(
                                key,
                                &serde_json::to_string(&commit).unwrap(),
                            ))
                            .send()
                            .expect("Failed to send request");
                    }

                    let mut file = OpenOptions::new()
                        .write(true)
                        .append(true)
                        .create(true)
                        .open(checksum_file)
                        .unwrap();

                    if let Err(e) = writeln!(
                        file,
                        "{}|{}",
                        checksum,
                        path.clone().into_os_string().into_string().unwrap()
                    ) {
                        println!("Failed to write checksum {}, because of {}", checksum, e);
                    }

                    checksums.insert(checksum, os_path.clone());
                }
            }
        }
    }

    Ok(())
}

fn append(key: &str, session: Session, offset: u64, data: Vec<u8>) {
    let client = reqwest::blocking::Client::new();
    let append_url =
        reqwest::Url::parse("https://content.dropboxapi.com/2/files/upload_session/append_v2")
            .expect("Failed to parse url");
    let append = AppendSession {
        cursor: Cursor {
            session_id: session.session_id,
            offset: offset,
        },
        close: false,
    };

    let _append = client
        .post(append_url)
        .headers(construct_session_headers(
            key,
            &serde_json::to_string(&append).unwrap(),
        ))
        .body(data)
        .send()
        .expect("Failed to send request");
}

fn construct_upload_headers(key: &str, api_arg: &str) -> HeaderMap {
    let mut list_headers = HeaderMap::new();
    list_headers.insert(
        CONTENT_TYPE,
        HeaderValue::from_static("application/octet-stream"),
    );

    list_headers.insert(AUTHORIZATION, HeaderValue::from_str(key).unwrap());
    list_headers.insert("Dropbox-API-Arg", HeaderValue::from_str(api_arg).unwrap());

    list_headers
}

fn construct_session_headers(key: &str, api_arg: &str) -> HeaderMap {
    let mut list_headers = HeaderMap::new();
    list_headers.insert(
        CONTENT_TYPE,
        HeaderValue::from_static("application/octet-stream"),
    );

    list_headers.insert(AUTHORIZATION, HeaderValue::from_str(key).unwrap());
    list_headers.insert("Dropbox-API-Arg", HeaderValue::from_str(api_arg).unwrap());

    list_headers
}

fn is_hidden(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s.starts_with("."))
        .unwrap_or(false)
}

fn start_download(key: &str, outdir: &str) -> Result<(), Box<dyn std::error::Error>> {
    let path = Path::new(".cursor");
    let mut cursor = String::new();

    if path.exists() {
        let mut file = File::open(path)?;
        file.read_to_string(&mut cursor)?;
    }

    if cursor.len() > 0 {
        let more = get_from_cursor(key, &cursor);
        cursor = more.cursor;

        if let Some(entry) = more.entries.get(0) {
            download(key, entry, outdir);
        }
    } else {
        if let Ok(e) = get_entries(key) {
            let more = get_from_entry_cursor(key, e);
            cursor = more.cursor;

            if let Some(entry) = more.entries.get(0) {
                download(key, entry, outdir);
            }
        }
    }

    if cursor.len() > 0 {
        let mut file = File::create(path)?;
        file.write_all(cursor.as_bytes())?;
    }

    Ok(())
}

fn get_key() -> String {
    match env::var("DBX_KEY") {
        Ok(var) => format!("Bearer {}", var),
        Err(_) => panic!("DBX_KEY not found in your environment!"),
    }
}

fn construct_list_headers(key: &str) -> HeaderMap {
    let mut list_headers = HeaderMap::new();
    list_headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));

    list_headers.insert(AUTHORIZATION, HeaderValue::from_str(key).unwrap());

    list_headers
}

fn get_entries(key: &str) -> Result<Entries, reqwest::Error> {
    let client = reqwest::blocking::Client::new();
    let url = reqwest::Url::parse("https://api.dropboxapi.com/2/files/list_folder")
        .expect("Failed to parse url");

    let mut body = HashMap::new();
    body.insert("path", "/camera uploads");

    let response = client
        .post(url)
        .headers(construct_list_headers(key))
        .json(&body)
        .send()
        .expect("Failed to send request");

    response.json::<Entries>()
}

fn get_from_cursor(key: &str, cursor: &str) -> Entries {
    let mut e = Entries::default();

    if e.has_more {
        let client = reqwest::blocking::Client::new();
        let url = reqwest::Url::parse("https://api.dropboxapi.com/2/files/list_folder/continue")
            .expect("Failed to parse url");

        let mut body = HashMap::new();
        body.insert("cursor", &cursor);

        let response = client
            .post(url)
            .headers(construct_list_headers(key))
            .json(&body)
            .send()
            .expect("Failed to send request");

        if let Ok(mut more) = response.json::<Entries>() {
            e.entries.append(&mut more.entries);
            e.has_more = more.has_more;
            e.cursor = more.cursor;
            e = get_from_entry_cursor(key, e);
        }
    }

    e
}

fn get_from_entry_cursor(key: &str, entries: Entries) -> Entries {
    let mut e = entries;

    if e.has_more {
        let client = reqwest::blocking::Client::new();
        let url = reqwest::Url::parse("https://api.dropboxapi.com/2/files/list_folder/continue")
            .expect("Failed to parse url");

        let mut body = HashMap::new();
        body.insert("cursor", &e.cursor);

        let response = client
            .post(url)
            .headers(construct_list_headers(key))
            .json(&body)
            .send()
            .expect("Failed to send request");

        if let Ok(mut more) = response.json::<Entries>() {
            e.entries.append(&mut more.entries);
            e.has_more = more.has_more;
            e.cursor = more.cursor;
            e = get_from_entry_cursor(key, e);
        }
    }

    e
}

fn download(key: &str, entry: &Entry, outdir: &str) {
    let client = reqwest::blocking::Client::new();
    let url = reqwest::Url::parse("https://content.dropboxapi.com/2/files/download")
        .expect("Failed to parse url");
    let mut headers = HeaderMap::new();

    headers.insert(AUTHORIZATION, HeaderValue::from_str(&key).unwrap());

    headers.insert(
        HeaderName::from_static("dropbox-api-arg"),
        HeaderValue::from_str(format!("{{\"path\": \"{}\"}}", entry.id).as_str()).unwrap(),
    );

    let response = client
        .post(url)
        .headers(headers)
        .send()
        .expect("Failed to send request");

    if let Ok(data) = response.bytes() {
        let path = Path::new(outdir);
        let mut path = path.join(&entry.name);

        if path.exists() {
            path = create_new_path(
                outdir,
                path.file_name().unwrap().to_str().unwrap(),
                path.extension().unwrap().to_str().unwrap(),
            );
        }

        let mut out = File::create(path).unwrap();
        io::copy(&mut data.as_ref(), &mut out).unwrap();
    }
}

fn create_new_path(outdir: &str, file_name: &str, ext: &str) -> PathBuf {
    let path = Path::new(outdir);
    let mut i = 1;
    let mut pb = PathBuf::new();

    loop {
        let new_file = format!("{} ({}).{}", file_name, i, ext);
        pb = path.join(new_file.as_str());

        if !pb.exists() {
            break;
        }
        i += 1;
    }

    pb
}
